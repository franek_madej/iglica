from entities import TavernBrawler, game_over
from utilities import (clear_screen, common_options, dispatch_action,
                       get_entities_options, get_movement_options,
                       get_quests_options, print_map, save_game)

BRAWLER_COUNT = 5


def tavern_quest(locations, game_state):
    if len(locations[0]['entities']) == 0:
        print()
        print('"Gratulacje! Wymiotłeś stąd tych łapserdaków."')
        print('Zdobywasz 300 punktów doświadczenia.')
        game_state['main_char'].gain_experience(300)
        game_state['tavern'] = True
        print()
        locations[0]['quests'].pop(0)
    else:
        brawlers_left = len(locations[0]['entities'])
        print('Zostało ci {} karczmianych bywalców!'.format(brawlers_left))


def meat_shop_quest(locations, game_state):
    print('Mięso nie wygląda tak dobrze... Czy chcesz je kupić?')
    answer = input('[TAK/NIE]: ')
    answer = answer == 'TAK'  # boolean
    print()
    if answer:
        print('Kupiłeś mięso! Sprzedawca jest bardzo szczęśliwy.')
        game_state['meat_shop_quest'] = True
    else:
        print('Odmawiasz... Sprzedawca jest załamany')
        game_state['meat_shop_quest'] = False
    locations[1]['quests'].pop(0)


def elder_quest(locations, game_state):
    character_name = game_state['main_char'].name
    point_count = 0
    choices = (
        'Wpadłem przypadkiem. Podróżuję bez celu.',
        'Szukam sposobu aby dostać się do wyższego poziomu.',
        'Spytaj raczej dlaczego to ty spotkałeś mnie...'
    )
    print('Witaj {}, co cię tu sprowadza?'.format(character_name))
    print()
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is not 2:
        print()
        print('Interesujące...')
        print()
        point_count += 1
    print('''\
Żyję w cierpieniu. Cały nasz poziom jest dziesiątkowany przez
epidemię. Nie stać nas na lekarstwa, nawet jeżeli to nie ma kto
udać się na wyższe poziomy by je przetransportować..
Pozwól że zmienię temat.'
Jestem wielkim fanem matematyki. Dasz radę mojej zagadce...?
To jedyne co mi pozostało w tych smutnych czasach.''')

    print()
    choices = (
        'Tak, co dwa lata.',
        'Tak, co pięć lat.',
        'Nie, nie jest to możliwe.',
    )
    message = '''\
Czy możliwe jest, by w danym roku nie było
piątku trzynastego? Jeśli tak, to co ile lat wystąpi
taka sytuacja?'''
    print(message)
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is 3:
        print()
        print('Doskonale!')
        print()
        point_count += 1
    else:
        print()
        print('Nie mam dobrych wiadomości. Taka sytuacja zdarzy się zawsze...')
        print()

    print()
    choices = (
        'Naprawdę wpadłem przypadkiem.',
        'Szukam sposobu aby dostać się do wyższego poziomu',
        'Dlaczego uważasz, że na początku nie mówiłem prawdy?',
    )
    message = '''\
Przejdźmy do rzeczy. Dlaczego tak naprawdę
jesteś tutaj?'''
    print(message)
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is not 1:
        point_count += 1

    if game_state['meat_shop_quest']:
        print()
        print('''\
Słyszałem, że kupiłeś mięso u naszego sklepikarza...
Biedak pracuje całe dnie ale nikt tego nie docenia.
Dziękuję.''')
        print()
        point_count += 1

    end_game = False
    if point_count > 2:
        print()
        print('''\
To była interesująca rozmowa. Weź moją przepustkę i idź do
kręgu mieszczan. Ten świat na Ciebie czeka.''')
        print('''\
Nie mogę sprawić, że pomożesz moim ludziom. To od Twojego serca
zależy co dalej zrobisz. Wierzę, że jesteś odpowiednim
człowiekiem na właściwym miejscu, dlatego oddałem ci
przepustkę.''')
        print()
        input('Naciśnij Enter aby kontynuować!')
        game_state['act'] = 2
        game_state['location'] = 0
        end_game = True
    else:
        print()
        print('''\
Zawiodłeś mnie swoimi odpowiedziami. Nie mogę dać ci mojej
przepustki.''')
        print()
        game_over(game_state)
    return end_game


def description():
    clear_screen()
    print('''\
Szukając sławy i pieniędzy w swoim życiu zawitałeś do Iglicy.
Na zlecenie Króla postarasz się pomóc mieszkańcom, którzy
są nękani epidemią ospy. Zaklęcie odporności jest niezwykle trudne
do opanowania, dlatego tylko bardzo doświadczeni magowie potrafią
je rzucić. Król opłacał śmiałków, którzy byli gotowi ruszyć pomóc
Iglicy. Jesteś jednym z nich.''')
    print()
    print('''\
Miasto jest położone przy granicy królestwa. Jest zbudowane na zboczu
góry i składa się z trzech poziomów. Pierwszy, najniższy,
przeznaczony jest dla miejscowej biedoty. Na drugi wejść może
tylko mieszczaństwo. Trzeci, najwyższy to teren szlachecki.
Rezyduje tam także rada miasta.''')
    print()
    input('Naciśnij Enter aby przejść dalej')


def first_act(game_state, locations):
    '''
     ___ ___
    |   |   |
    | 1 | 2 |
    |___|___|___
        |   |   |
        | 3 | 4 |
        |___|___|
        |   |
        | 5 |
        |___|

    1. Karczma
    2. Sklep mięsny
    3. Rynek
    4. Dom starosty
    5. Dzielnica mieszkalna

    '''

    if not locations:
        locations = (
            {
                'name': 'Karczma pod Świnskim Łbem',
                'entities': [
                ],
                'up': None,
                'left': None,
                'right': 1,
                'down': None,
                'coords': (0, 0),
                'description': '''\
Karczma pod Świnskim Łbem to miejsce ciągłej bitki. Jeżeli chcesz
żeby ktoś wyklepał ci twarz - trafiłeś idealnie. Po prostu
zamachnij się na któregoś z bywalców i niech rozpęta się burda.
Jeżeli ci to nie w smak - lepiej szybko uciekaj!
                ''',
                'quests': [
                    {
                        'description': '''\
Zamiast na gości karczmy zwracasz się do barmana. "Dobry człowieku!
Pozbądź się ich wreszcie! Nie mogę już wytrzymać, ciągle tylko się tłuką!".
Pomożesz biedakowi? Pozbądź się wszystkich karczmianych rzezimieszków.''',
                        'validate': tavern_quest,
                    }
                ]
            },
            {
                'name': 'Sklep mięsny',
                'entities': [
                ],
                'up': None,
                'left': 0,
                'right': None,
                'down': 2,
                'coords': (0, 1),
                'description': '''\
Jak na standardy najniższego poziomu miasta, sklep wydaje się być niezwykle
ładnie utrzymany... Sprzedawca uśmiecha się przyjaźnie.
Może warto kupić u niego mięso?''',
                'quests': [
                    {
                        'description': 'Kup mięso od miłego pana!',
                        'validate': meat_shop_quest,
                    }
                ]
            },
            {
                'name': 'Rynek',
                'entities': [
                ],
                'up': 1,
                'left': None,
                'right': 3,
                'down': 4,
                'coords': (1, 1),
                'description': '',
                'quests': [],
            },
            {
                'name': 'Dzielnica mieszkalna',
                'entities': [
                ],
                'up': None,
                'left': 2,
                'right': None,
                'down': None,
                'coords': (1, 2),
                'description': '',
                'quests': [],
            },
            {
                'name': 'Dom starosty',
                'entities': [
                ],
                'up': 2,
                'left': None,
                'right': None,
                'down': None,
                'coords': (2, 1),
                'description': '''
Dotarłeś do domu starosty. Jest on jednym z niewielu starszych na tym
poziomie miasta. Niektórzy twierdzą, że dożył swojego wieku tylko
dlatego, że dzieciństwo spędził w wyższych rejonach. Starosta słynie ze
swojej mądrości i tego, że zawsze ma na celu pomoc rejonowi
najbiedniejszych.''',
                'quests': [
                    {
                        'description': 'Czas na rozmowę ze starostą',
                        'validate': elder_quest,
                    }
                ]

            },
        )

        # Brawlers initialization
        for i in range(BRAWLER_COUNT):
            locations[0]['entities'].append(TavernBrawler())

    description()

    clear_screen()
    print('''\
Poniżej mapa najniższego poziomu Iglicy. Dostęp do wyższych sfer
nie jest łatwy. Musisz poszukać osób, które ci go zapewnią.''')
    print_map(locations, game_state['location'])
    input('Naciśnij Enter aby przejść dalej')

    while True:
        clear_screen()
        current_location = locations[game_state['location']]['name']
        character_name = game_state['main_char'].name
        print('{}, jesteś w "{}"'.format(character_name, current_location))
        print()
        available_locations = get_movement_options(game_state, locations)
        available_entities = get_entities_options(game_state['location'],
                                                  locations)
        available_quests = get_quests_options(game_state['location'],
                                              locations)
        choices = (available_entities
                   + ('-' * 15, ) + available_quests
                   + ('-' * 15, ) + available_locations
                   + ('-' * 15, ) + common_options)
        for index, option in enumerate(choices):
            print('{}. {}'.format(index + 1, option))
        print()
        answer = int(input('Co wybierasz? [1, 2, 3...]: '))
        end_game = dispatch_action(choices[answer - 1], game_state, locations)
        if end_game:
            break

    # Go to Act 2
    game_state['act'] = 2
    save_game(game_state, None)
    return game_state
