from entities import JewelThief, game_over
from utilities import (clear_screen, common_options, dispatch_action,
                       get_entities_options, get_movement_options,
                       get_quests_options, print_map, save_game)

JEWEL_THIEFS = 3


def jewel_quest(locations, game_state):
    if len(locations[0]['entities']) == 0:
        print()
        print('"Tak bardzo Ci dziękuję! Sam nigdy nie dałbym rady uratować '
              'sklepu."')
        print('"Weź ten pierścień, który magowie zaklęli czarami zwinności '
              'i inteligencji')
        game_state['main_char'].intelligence += 3
        game_state['main_char'].dexterity += 3
        game_state['ring'] = True
        print()
        locations[1]['quests'].pop(0)
    else:
        print('Zawiodłeś mnie! Złodzieje zdążyli uciec... Co ja pocznę!')
        locations[1]['quests'].pop(0)


def park_quest(locations, game_state):
    print('''\
Fontanna obrazująca walkę ze smokiem stoi pośrodku placu.
Czy chcesz wrzucić monetę na szczęście?''')
    print()
    answer = input('[TAK/NIE]: ')
    answer = answer == 'TAK'  # boolean
    if answer:
        print('Może na coś się to zda?')
        game_state['main_char'].dexterity += 2
        input('Naciśnij Enter aby kontynuować!')
    else:
        print('Tylko ludzie ograniczeni wierzą w zabobony.')
        game_state['main_char'].intelligence += 2
        input('Naciśnij Enter aby kontynuować!')
    locations[2]['quests'].pop(0)


def court_quest(locations, game_state):
    print('Rozpoczynam posiedzenie!')
    print()
    print('W sprawie Leopolda Nowaka, który bez pozwolenia wszedł na'
          'poziom szlachecki.')
    print()
    print('Czy masz coś na swoją obronę?')
    print()
    print('Nic Wysoki Sądzie.')
    print()
    print('Orzekam karę pozbawienia wolności dla strażnika, który przyjął '
          'od Leopolda łapówkę.')
    input('Naciśnij Enter aby kontynuować!')

    clear_screen()
    print('''
Rozglądasz się po pomieszczeniu. Okazuje się, że jedno z wyjść
prowadzi na poziom szlachecki. Pilnuje go jednak strażnik.
nie masz przepustki, więc możesz zostać ukarany za próbę
przekupstwa. Jak sam widziałeś, zasady zostały zaostrzone.

Czy chcesz z nim rozmawiać?''')
    answer = input('[TAK/NIE]: ')
    answer = answer == 'TAK'  # boolean
    if not answer:
        return False
    choices = (
        'Proszę przepuść mnie, mam chorą córkę i muszę ją ratować.',
        'Jeżeli przepuścisz mnie wyżej to przyniosę Ci co tylko chcesz.',
        'Weź to złoto...'
    )
    ring_answer = 'Weź ten pierścień, na pewno spodoba się twojej żonie...'
    if 'ring' in game_state:
        choices = choices + (
            ring_answer,
        )
    print('Strażniku, musisz mnie przepuścić!')
    print()
    print('Niby dlaczego?')
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3...]: '))

    end_game = False
    if answer is 4 and 'ring' in game_state:
        print()
        print('''\
To pierścień od naszego jubilera? Moja żona zawsze o takim marzyła...
Przechodź szybko. I nigdy nikomu o tym nie wspominaj!''')
        print()
        input('Naciśnij Enter aby kontynuować!')
        game_state['act'] = 3
        game_state['location'] = 0
        end_game = True
    elif game_state['main_char'].intelligence > 14:
        print('Dobra przechodź. Nigdy nikomu o tym nie wspominaj.')
        print()
        input('Naciśnij Enter aby kontynuować!')
        game_state['act'] = 3
        game_state['location'] = 0
        end_game = True
    else:
        print('Próbujesz przekupstwa, namawiania? Do więzienia z Tobą!')
        print()
        game_over(game_state)
    return end_game


def description():
    clear_screen()
    print('''\
Udało się! Dostałeś się na drugi poziom miasta.
To rejon mieszczański. Zaraza nie rozprzestrzenia się tutaj tak jak poziom
niżej, niektórzy ludzie są też pod wpływem magii odpornościowej.

Warto odwiedzić Park i Jubilera, z których środkowe piętro słynie w okolicy.\
''')
    print()
    input('Naciśnij Enter aby przejść dalej')


def second_act(game_state, locations):
    '''
     ___ ___ ___
    |   |   |   |
    | 2 | 3 | 4 |
    |___|___|___|
    |   |   |   |
    | 1 |   | 5 |
    |___|   |___|

    1. Kościół
    2. Jubiler
    3. Park
    4. Dzielnica mieszkalna
    5. Sąd

    '''
    if not locations:
        locations = (
            {
                'name': 'Kościół',
                'entities': [
                ],
                'up': 1,
                'left': None,
                'right': None,
                'down': None,
                'coords': (1, 0),
                'description': '''\
W tych czasach nikt nie odwiedza już kościołów...''',
                'quests': [
                ]
            },
            {
                'name': 'Jubiler',
                'entities': [
                ],
                'up': None,
                'left': None,
                'right': 2,
                'down': 0,
                'coords': (0, 0),
                'description': '''\
Sklep jubilerski rejonu mieszczańskiego słynie w okolicy z doskonałej jakości
wyrobów oraz terminowości i temperamentu własciciela.''',
                'quests': [
                    {
                        'description': '''
RATUNKU! Wpadli złodzieje i okradają mój sklep, wszystko co mam! Proszę, pomóż
mi ich zatrzymać!''',
                        'validate': jewel_quest,
                    }
                ]
            },
            {
                'name': 'Park',
                'entities': [
                ],
                'up': None,
                'left': 1,
                'right': 3,
                'down': None,
                'coords': (0, 1),
                'description': '''
Niezwykle zadbany park środkowego poziomu Iglicy jest oczkiem w głowie kasty
mieszczan. Zwłaszcza fontanna, swego rodzaju atrakcja dla podróżników,
przedstawia jednego z magów podczas walki ze smokiem. To właśnie to zdarzenie
było jednym z najważniejszych w historii miasta.''',
                'quests': [
                    {
                        'description': 'Czas zobaczyć fontannę.',
                        'validate': park_quest,
                    }
                ],
            },
            {
                'name': 'Dzielnica mieszkalna',
                'entities': [
                ],
                'up': None,
                'left': 2,
                'right': None,
                'down': 4,
                'coords': (0, 2),
                'description': '',
                'quests': [],
            },
            {
                'name': 'Sąd',
                'entities': [
                ],
                'up': 3,
                'left': None,
                'right': None,
                'down': None,
                'coords': (1, 2),
                'description': '''
Mieszczański sąd jest jedną z najważniejszych instytucji na tym poziomie.
                ''',
                'quests': [
                    {
                        'description': 'Za chwilę rozpoczyna się rozprawa. '
                        'Masz chwilę aby posłuchać?',
                        'validate': court_quest,
                    }
                ]

            },
        )
        for i in range(JEWEL_THIEFS):
            locations[1]['entities'].append(JewelThief())

    description()

    clear_screen()
    print('Poniżej mapa mieszczańskiego poziomu Iglicy.')
    print_map(locations, game_state['location'])
    input('Naciśnij Enter aby przejść dalej')

    while True:
        clear_screen()
        current_location = locations[game_state['location']]['name']
        character_name = game_state['main_char'].name
        print('{}, jesteś w "{}"'.format(character_name, current_location))
        print()
        available_locations = get_movement_options(game_state, locations)
        available_entities = get_entities_options(game_state['location'],
                                                  locations)
        available_quests = get_quests_options(game_state['location'],
                                              locations)
        choices = (available_entities
                   + ('-' * 15, ) + available_quests
                   + ('-' * 15, ) + available_locations
                   + ('-' * 15, ) + common_options)
        for index, option in enumerate(choices):
            print('{}. {}'.format(index + 1, option))
        print()
        answer = int(input('Co wybierasz? [1, 2, 3...]: '))
        end_game = dispatch_action(choices[answer - 1], game_state, locations)
        if end_game:
            break

    # Go to Act 3
    game_state['act'] = 3
    save_game(game_state, None)
    return game_state
