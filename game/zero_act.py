from utilities import save_game, clear_screen
from entities import MainCharacter


def zero_act(game_state, locations):
    clear_screen()
    print('Witaj w świecie Iglicy podróżniku. '
          'Zacznijmy od paru słów o Tobie!')
    main_char = MainCharacter()
    main_char.__str__()
    print('Dziękuję. Wszystko to zostanie zapisane w mojej księdze. '
          'Wstrzymaj się na kilka chwil...')
    game_state['main_char'] = main_char

    # Go to Act 1
    game_state['act'] = 1
    save_game(game_state, None)
    return game_state
