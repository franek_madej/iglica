from entities import game_over
from utilities import (clear_screen, common_options, dispatch_action,
                       get_entities_options, get_movement_options,
                       get_quests_options, print_map)


def hall_quest(locations, game_state):
    character_name = game_state['main_char'].name
    print('Do roboty {}... Co przedstawiają te obrazy?'.format(character_name))
    choices = (
        'Wygnanie smoka z góry stanowiącej podłoże dla Iglicy.',
        'Pierwszy Król ratuje swoje królestwo.',
    )
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is 1:
        print()
        print('Dobra robota! Warto było słuchać opowieści w dzieciństwie.')
        print()
        game_state['main_char'].intelligence += 2
    else:
        print('Niestety, to nie to zdarzenie. Co ciekawe to o którym mówisz '
              ' nigdy nie miało miejsca...')
    choices = (
        'Król, Który Zjednoczył i jego haniebna młodość.',
        'Przyprawa Gołebia, kolebka handlu w królestwie.',
    )
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is 2:
        print()
        print('Trudno zapomnieć potrawy własnej matki.')
        print()
        game_state['main_char'].intelligence += 2
    else:
        print('To przykre, że hańbisz najwspanialszego władcę królestwa')

    print()
    print('Starczy tej zabawy. Czas ruszać dalej!')
    print()
    input('Naciśnij Enter aby kontynuować!')
    locations[1]['quests'].pop(0)


def garden_quest(locations, game_state):
    point_count = 0
    character_name = game_state['main_char'].name
    print('Pierwsza dama Iglicy przebywa samotnie w ogrodzie.'
          ' Decydujesz się podejść i z nią porozmawiać.')
    print()
    print('Kim jesteś młodzieńcze? Co Cię tu sprowadza?')
    print()
    choices = (
        'Jestem {}. Przybywam z nadania Króla. '.format(character_name),
        'Chcę pomóc mieszkańcom w obliczu zarazy.',
        'Jestem łowcą nagród. Król opłaca śmiałków, jestem jednym '
        'z nich',
    )
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is choices[0]:
        print('Brakuje ludzi o dobrym sercu... Czy faktycznie jesteś jednym z '
              'nich?')
        print()
    else:
        point_count += 1
        print('Nie wierzę w łowców nagród. Nie byłoby Cię '
              'tutaj, gdybyś nie chciał pomagać.')
        print()

    print('Słyszałam o kimś, kto pomagał na wszystkich poziomach mieszkańcom '
          'miasta')
    print('''
Podobno ten śmiałek pokonał bandytów, chcących ukraść jubilera,
wygonił awanturników z Karczmy i na dodatek kupił mięso mimo
zarazy.''')
    print()
    print('Czy to byłeś ty?')
    answer = input('[TAK/NIE]: ')
    answer = answer == 'TAK'  # boolean
    print()
    chosen_one = (game_state['meat_shop_quest']
                  and game_state['ring']
                  and game_state['tavern'])
    if answer and chosen_one:
        point_count += 2
        print('Imponujące')
    elif not answer and not chosen_one:
        point_count += 1
        print('Doceniam szczerość, zwłaszcza gdy kłamstwo wydaje się '
              'łatwą drogą do sukcesu')
    elif not answer and chosen_one:
        point_count += 3
        print('Skromność to najważniejsza cecha. Od początku znałam prawdę, '
              'chciałam sprawdzić twoją prawdomówność')
    else:
        point_count -= 1
        print('Zawiodłam się na Tobie.')
    print()

    if point_count >= 2:
        print('Jesteś wyjątkowym człowiekiem {}'.format(character_name))
        print()
        print('Wiem, że jesteś tu by pomóc miastu. Porozmawiam z moim mężem.')
        print()
        print('Odwiedź go w sali tronowej.')
        game_state['queen'] = True
    else:
        print('Pomyliłam się co do Ciebie.')
        print()
        print('Muszę ruszać. Żegnaj')
    print()
    locations[2]['quests'].pop(0)


def final_quest(locations, game_state):
    print('Król patrzy na Ciebie.')
    print()
    print('Niegdyś Iglica była najważniejszym miastem królestwa, '
          'dorównywała jej jedynie stolica a i tak wielu nie chciało'
          'się z tym zgodzić.')
    print()
    print('Od kiedy uderzyła w nas plaga ospy, miasto zaczęło powoli umierać.'
          'Nasi wrogowie wiedzą o tym, zawiadowcy doniesli mi o wymarszu wojsk'
          ' naszego sąsiada... Jeżeli tak można go jeszcze nazwać. '
          'Niedługo nie będzie czego bronić, wjadą do martwego miasta.')
    print()
    print('')
    print()
    character_name = game_state['main_char'].name
    choices = (
        '''\
{}. Przybywam z misją ratunkową. Poznałem już poszczególne poziomy miasta.
Musisz uratować biedotę i zrekrutować do swojej rady jej starostę.
Zbyt długo żyją oni na marginesie, jednocześnie będąc
największą grupą wśród mieszkańców.'''.format(character_name),
        '''\
{}. Przybywam z misją ratunkową. Poznałem już poszczególne poziomy miasta.
Musisz uzbroić mieszczan. Środkowy poziom rozwinął się i może być jedyną
drogą by uratować miasto przed wrogiem z zewnątrz.'''.format(character_name),
    )
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))
    if answer is 1:
        print('Biedota jest powodem zarazy. Nie mogę tego zrobić.')
    else:
        print('Co mi z tego, że obronimy miasto przed najeźdzcą? Zaraz wybije '
              'nas co do jednego.')
    print()
    choices = (
        'Panie, nie możesz teraz się poddawać. '
        'Twoi mieszkańcy Cię potrzebują.',
        '''\
Rozmawiałem z królową. Musisz walczyć dalej, dla niej i dla twoich ludzi.
Uzbrój mieszczaństwo i biedotę, niech oni walczą. Szlachta musi opłacić
lekarstwa dla niższych warstw.''',
    )
    for index, option in enumerate(choices):
        print('{}. {}'.format(index + 1, option))
    print()
    answer = int(input('Jaka jest twoja odpowiedź? [1, 2, 3]: '))

    print()
    if answer is 1:
        print('''\
Niegdyś byłem władcą, który odpowiedziałby na takie wezwanie.

Ale dziś, jestem tylko słabym, wyniszczonym człowiekiem.

Wyjedź, póki jeszcze możesz.''')
    else:
        if 'queen' in game_state and game_state['queen']:
            print('To prawda. Muszę walczyć dla niej, muszę uratować moich '
                  'ludzi. Dziękuję, że otworzyłeś mi oczy.')
        else:
            print('Jestem na końcu życia. Dlatego nie ukarzę Cię za to '
                  'bezwstydne kłamstwo. Odejdź póki jeszcze możesz.')
            print()
            game_over(game_state)
    print()
    input('Naciśnij Enter aby kontynuować!')
    game_state['act'] = 4
    game_state['location'] = 0
    end_game = True
    return end_game


def description():
    clear_screen()
    print('''\
Udało Ci się pokonać przeszkodę w postaci strażnika. Dzięki temu dostałeś się
na zamek władcy Iglicy. O ile na najwyższym poziomie są także domostwa
szlachty, to czujesz że tutaj możesz dokończyć swoją misję
 związaną z miastem.''')
    print()
    input('Naciśnij Enter aby przejść dalej')


def third_act(game_state, locations):
    '''
        ___
       |   |
       | 5 |
    ___|___|___
   |   |   |   |
   | 4 | 2 | 3 |
   |___|___|___|
       |   |
       | 1 |
       |___|

    1. Brama
    2. Hol
    3. Palmiarnia
    4. Ogród
    5. Sala audiencyjna
    '''
    if not locations:
        locations = (
            {
                'name': 'Brama',
                'entities': [
                ],
                'up': 1,
                'left': None,
                'right': None,
                'down': None,
                'coords': (1, 2),
                'description': '''\
Brama zamku. Tutaj rezydują najważniejsi ludzie Iglicy.''',
                'quests': [
                ]
            },
            {
                'name': 'Hol',
                'entities': [
                ],
                'up': 4,
                'left': 3,
                'right': 2,
                'down': 0,
                'coords': (1, 1),
                'description': '''\
Władca iglicy jest bardzo zadowolony ze swojej kolekcji obrazów. Można je
oglądać właśnie tutaj, w zamkowym holu.''',
                'quests': [
                    {
                        'description': '''
Masz chwilę aby przypomnieć sobie historię królestwa?''',
                        'validate': hall_quest,
                    }
                ]
            },
            {
                'name': 'Ogród',
                'entities': [
                ],
                'up': None,
                'left': 1,
                'right': None,
                'down': None,
                'coords': (2, 1),
                'description': '''
Kolejnym ważnym dla arystokracji miejscem są zamkowe ogrody. Miejscy plotkarze
twierdzą, że można tu nawet spotkać samą królową, która bardzo
lubi zapach kwiatów.''',
                'quests': [
                    {
                        'description': 'To właśnie królowa! Chcesz z nią '
                        'porozmawiać?',
                        'validate': garden_quest,
                    }
                ],
            },
            {
                'name': 'Palmiarnia',
                'entities': [
                ],
                'up': None,
                'left': None,
                'right': 1,
                'down': None,
                'coords': (0, 1),
                'description': 'Palmiarnia jest nieco egzotycznym miejscem. '
                ' Ze względu na klimat Iglicy, rośliny żyją tylko dzięki '
                'magii.',
                'quests': [],
            },
            {
                'name': 'Sala tronowa',
                'entities': [
                ],
                'up': None,
                'left': None,
                'right': None,
                'down': 1,
                'coords': (1, 0),
                'description': '''
Zazwyczaj dobrze strzeżona, sala tronowa dzisiaj stoi otworem. Przez uchylone
drzwi dostrzegasz władcę Iglicy, który zasiada na tronie.''',
                'quests': [
                    {
                        'description': 'Władca wygląda na kompletnie '
                        'pogrążonego w smutku. '
                        'Może czas z nim porozmawiać?',
                        'validate': final_quest,
                    }
                ]

            },
        )

    description()

    clear_screen()
    print('Dostałeś się na zamek władcy. Oto rozmieszczenie poszczególnych '
          'sal.')
    print_map(locations, game_state['location'])
    input('Naciśnij Enter aby przejść dalej')

    while True:
        clear_screen()
        current_location = locations[game_state['location']]['name']
        character_name = game_state['main_char'].name
        print('{}, jesteś w "{}"'.format(character_name, current_location))
        print()
        available_locations = get_movement_options(game_state, locations)
        available_entities = get_entities_options(game_state['location'],
                                                  locations)
        available_quests = get_quests_options(game_state['location'],
                                              locations)
        choices = (available_entities
                   + ('-' * 15, ) + available_quests
                   + ('-' * 15, ) + available_locations
                   + ('-' * 15, ) + common_options)
        for index, option in enumerate(choices):
            print('{}. {}'.format(index + 1, option))
        print()
        answer = int(input('Co wybierasz? [1, 2, 3...]: '))
        end_game = dispatch_action(choices[answer - 1], game_state, locations)
        if end_game:
            break

    # Go to Act 3
    game_state['act'] = 4
    return game_state
