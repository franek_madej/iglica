import os
import sys
from random import choice, randint, uniform
from time import sleep

classes = (
    'Strzelec',
    'Mag',
    'Rycerz',
)

MIN_SKILL = 1
MID_SKILL = 5
UPP_SKILL = 10
MAX_SKILL = 15

polish_names = (
    'Jan',
    'Stanisław',
    'Andrzej',
    'Józef',
    'Tadeusz',
    'Jerzy',
    'Zbigniew',
    'Krzysztof',
    'Henryk',
    'Ryszard',
    'Kazimierz',
    'Marek',
    'Marian',
    'Piotr',
    'Janusz',
    'Władysław',
    'Adam',
    'Wiesław',
    'Zdzisław',
    'Edward',
)


def game_over(game_state):
    os.system('cls' if os.name == 'nt' else 'clear')
    print('To koniec twojej przygody. Następnym razem mierz siłę na '
          'zamiary, nie zamiar podług sił.')
    print()
    print('Oto twoje końcowe statystyki:')
    print()
    game_state['main_char'].description()
    sys.exit(0)


class Entity:
    level = 1
    name = 'Unnamed Entity'
    entity_type = 'Simple Entity'
    strength = 1
    dexterity = 1
    intelligence = 1
    hp = 15
    exp = 0

    def description(self):
        print()
        print(f'Jesteś {self.name}, {self.entity_type}!')
        print()
        print('Twoje statystyki:')
        print()
        print(f'Zręczność: {self.dexterity}')
        print(f'Siła: {self.strength}')
        print(f'Inteligencja: {self.intelligence}')
        print()

    def reset_health_points(self):
        self.hp = self.strength * 8 + self.dexterity * 4

    def calculate_damage_done(self):
        damage_done = int(
            (self.strength * 0.6 + self.intelligence * 0.4)
            * uniform(0.75, 1.25)
        )
        return damage_done

    def calculate_dodge(self):
        missed_attack = (randint(1, 7500)
                         + self.dexterity
                         + self.intelligence
                         > 7000)
        return missed_attack

    def calculate_experience_gain(self):
        return self.strength + self.dexterity + self.intelligence

    def level_up(self):
        print()
        print('Zyskujesz poziom!')
        self.strength += randint(MIN_SKILL, MID_SKILL)
        self.dexterity += randint(MIN_SKILL, MID_SKILL)
        self.intelligence += randint(MIN_SKILL, MID_SKILL)
        self.description()

    def gain_experience(self, gain):
        self.exp += gain
        if self.exp > self.level * 100:
            self.exp -= self.level * 100
            self.level_up()

    def __str__(self):
        return f'{self.name}, {self.entity_type}'


class TavernBrawler(Entity):
    def __init__(self):
        self.name = choice(polish_names)
        self.entity_type = 'Tawerniany zabijaka'
        self.strength = randint(MIN_SKILL, UPP_SKILL)
        self.dexterity = randint(MIN_SKILL, UPP_SKILL)
        self.intelligence = randint(MIN_SKILL, UPP_SKILL)


class JewelThief(Entity):
    def __init__(self):
        self.name = choice(polish_names)
        self.entity_type = 'Złodziej biżuterii'
        self.strength = randint(MIN_SKILL, UPP_SKILL)
        self.dexterity = randint(MIN_SKILL, UPP_SKILL)
        self.intelligence = randint(MIN_SKILL, UPP_SKILL)


class MainCharacter(Entity):
    def __init__(self):
        print()
        name = input('Jak się nazywasz podróżniku? ')
        print()
        print('Kim jesteś z zawodu?')
        for index, option in enumerate(classes):
            print('{}. {}'.format(index + 1, option))
        answer = int(input('Kim? [1, 2, 3...]: '))
        self.name = name
        self.entity_type = classes[answer - 1]

        '''
        Random stats initialization.
        '''
        if self.entity_type == classes[0]:
            self.strength = randint(MIN_SKILL, MAX_SKILL)
            self.dexterity = randint(UPP_SKILL, MAX_SKILL)
            self.intelligence = randint(MID_SKILL, MAX_SKILL)
        if self.entity_type == classes[1]:
            self.strength = randint(MIN_SKILL, MAX_SKILL)
            self.intelligence = randint(UPP_SKILL, MAX_SKILL)
            self.dexterity = randint(MID_SKILL, MAX_SKILL)
        if self.entity_type == classes[2]:
            self.intelligence = randint(MIN_SKILL, MAX_SKILL)
            self.strength = randint(UPP_SKILL, MAX_SKILL)
            self.dexterity = randint(MID_SKILL, MAX_SKILL)


def battle_entities(first_entity, second_entity, game_state):
    first_entity.reset_health_points()
    second_entity.reset_health_points()

    first_entity_won = True

    print(f'Walka między {first_entity.__str__()} a {second_entity.__str__()}')
    print()
    input('Naciśnij Enter aby kontynuować!')

    while True:
        # first_entity attacks first
        # calculate miss / dodge chance
        print(f'{first_entity.name} atakuje!')
        print()
        sleep(0.06)
        print()
        missed_attack = second_entity.calculate_dodge()
        if not missed_attack:
            damage_done = first_entity.calculate_damage_done()
            second_entity.hp -= damage_done
            print(f'I zadaje {damage_done} obrażeń. {second_entity.name} ma '
                  f'jeszcze {second_entity.hp} punktów życia!')
            if second_entity.hp <= 0:
                break
        else:
            print(f'{second_entity.name} unika ataku!')
        print()
        input('Naciśnij Enter by kontynuować!')
        print()
        # second_entity attacks second
        # calculate miss / dodge chance
        print(f'{second_entity.name} atakuje!')
        print()
        sleep(0.06)
        print()
        missed_attack = first_entity.calculate_dodge()
        if not missed_attack:
            damage_done = second_entity.calculate_damage_done()
            first_entity.hp -= damage_done
            print(f'I zadaje {damage_done} obrażeń. {first_entity.name} ma '
                  f'jeszcze {first_entity.hp} punktów życia!')
            if first_entity.hp <= 0:
                first_entity_won = False
                break
        else:
            print(f'{first_entity.name} unika ataku!')
        print()
        input('Naciśnij Enter by kontynuować!')
        print()

    first_entity.reset_health_points()
    second_entity.reset_health_points()
    if first_entity_won:
        print()
        print(f'{first_entity.name} zwycięża!')
        first_entity.gain_experience(second_entity.calculate_experience_gain())
        print()
        print(f'Po tej walce ma {first_entity.exp} punktów doświadczenia!')
        print()
        input('Naciśnij Enter aby kontynuować!')
    else:
        game_over(game_state)

    return first_entity_won
