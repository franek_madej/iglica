from utilities import clear_screen, load_game, print_help

from game.zero_act import zero_act
from game.first_act import first_act
from game.second_act import second_act
from game.third_act import third_act

NEW_GAME = 1
LOAD_GAME = 2
HELP = 3
EXIT = 4


def display_menu():
    clear_screen()
    print('''\
|    ___  ________  ___       ___  ________  ________       |
|   |\  \|\   ____\|\  \     |\  \|\   ____\|\   __  \      |
|   \ \  \ \  \___|\ \  \    \ \  \ \  \___|\ \  \|\  \     |
|    \ \  \ \  \  __\ \  \    \ \  \ \  \    \ \   __  \    |
|     \ \  \ \  \|\  \ \  \____\ \  \ \  \____\ \  \ \  \   |
|      \ \__\ \_______\ \_______\ \__\ \_______\ \__\ \__\  |
|       \|__|\|_______|\|_______|\|__|\|_______|\|__|\|__|  |
''')
    menu_options = ('Nowa Gra', 'Wczytaj Grę', 'Pomoc', 'Wyjdź')
    for index, option in enumerate(menu_options):
        print('{}. {}'.format(index + 1, option))
    print()
    return int(input('którą opcję wybierasz? '))


def finish_screen(game_state):
    print('Ukończyłeś Iglicę.')
    print()
    print('Oto statystyki końcowe twojego bohatera.')
    print()
    game_state['main_char'].description()
    print()
    print('Uratowałeś miasto!')


if __name__ == '__main__':
    game_state = {
        'main_char': None,
        'act': 0,
        'location': 0,
        'meat_shop_quest': False,
        'ring': False,
        'tavern': False
    }
    locations = None
    choice = display_menu()
    if choice == LOAD_GAME:
        game_state, locations = load_game()

    if choice == HELP:
        print_help()
        choice = display_menu()

    if choice == EXIT:
        exit(0)

    if choice == NEW_GAME or game_state['act'] == 0:
        game_state = zero_act(game_state, locations)
    if game_state['act'] == 1:
        game_state = first_act(game_state, locations)
    if game_state['act'] == 2:
        game_state = second_act(game_state, locations)
    if game_state['act'] == 3:
        game_state = third_act(game_state, locations)
    if game_state['act'] == 4:
        finish_screen(game_state)
