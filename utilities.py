import os
import pickle
import re
import sys

from entities import battle_entities

GAME_STATE_FILE = 'game_state.data'
LOCATIONS_FILE = 'locations.data'


def get_movement_options(game_state, locations):
    current_location = game_state['location']
    current_location = locations[current_location]
    available_locations = []
    if current_location['up'] is not None:
        available_locations.append(current_location['up'])
    if current_location['left'] is not None:
        available_locations.append(current_location['left'])
    if current_location['right'] is not None:
        available_locations.append(current_location['right'])
    if current_location['down'] is not None:
        available_locations.append(current_location['down'])

    result = []
    for location in available_locations:
        option_name = locations[location]['name']
        result.append(f'Ruch: {location + 1}. {option_name}')

    return tuple(result)


common_options = (
    'Informacje o obecnej lokacji',
    'Informacje o bohaterze',
    'Wyświetl mapę poziomu',
    'Zapisz',
    'Zakończ',
)


def get_entities_options(current_location, locations):
    entities = locations[current_location]['entities']
    result = []
    for index, entity in enumerate(entities):
        result.append(f'Walcz {index + 1}: {entity.__str__()}')
    return tuple(result)


def get_quests_options(current_location, locations):
    quests = locations[current_location]['quests']
    result = []
    for index, entity in enumerate(quests):
        result.append(f'Informacje o zadaniu {index + 1}')
        result.append(f'Sprawdź zadanie {index + 1}')
    return tuple(result)


def dispatch_action(message, game_state, locations):
    move = re.match(r'Ruch: ([0-9])', message)
    fight = re.match(r'Walcz ([0-9])', message)
    quest_info = re.match(r'Informacje o zadaniu ([0-9])', message)
    validate = re.match(r'Sprawdź zadanie ([0-9])', message)

    location = locations[game_state['location']]
    quests = location['quests']

    if move:
        game_state['location'] = int(move.groups()[0]) - 1
    if fight:
        if battle_entities(game_state['main_char'],
                           location['entities'][int(fight.groups()[0]) - 1],
                           game_state):
            location['entities'].pop(int(fight.groups()[0]) - 1)

    clear_screen()

    if quest_info:
        print(quests[int(quest_info.groups()[0]) - 1]['description'])
    if validate:
        validate = quests[int(validate.groups()[0]) - 1]['validate']
        end_game = validate(locations, game_state)
        if end_game:
            return True

    if message == common_options[0]:
        print(locations[game_state['location']]['description'])

    if message == common_options[1]:
        game_state['main_char'].description()

    if message == common_options[2]:
        print_map(locations, game_state['location'])

    if message == common_options[3]:
        save_game(game_state, locations)

    if message == common_options[4]:
        sys.exit(0)

    print()
    input('Naciśnij Enter aby przejść dalej')
    return False


def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')


def print_help():
    print()
    print('''\
Iglica jest tekstową grą RPG. Spokojnie wybieraj i wpisz numer decyzji gdy jej
dokonasz. Twoje decyzje wpływają na kształt świata i dalszej przygody
dlatego zastanów się porządnie.

System walki jest prosty i bardzo losowy. Unikaj starć z silniejszymi
od siebie rywalami. Staraj się natomiast walczyć bo awans z poziomu
na poziom może niezwykle Cię wzmocnić.''')
    print()
    input('Naciśnij Enter aby kontynuować!')


def load_game():
    f = open(GAME_STATE_FILE, 'rb')
    game_state = pickle.load(f)
    f = open(LOCATIONS_FILE, 'rb')
    locations = pickle.load(f)
    return game_state, locations


def save_game(game_state, locations):
    f = open(GAME_STATE_FILE, 'wb')
    pickle.dump(game_state, f)
    f = open(LOCATIONS_FILE, 'wb')
    pickle.dump(locations, f)
    print('Saved!')


def merge_renders(first_render, second_render):
    first_render_list = list(first_render)
    second_render_list = list(second_render)
    for i in range(len(first_render_list)):
        if first_render_list[i] == ' ' and i < len(second_render_list):
            first_render_list[i] = second_render_list[i]
    return ''.join(first_render_list)


def print_map(locations, current_location):
    '''
    Prints location map based on coordinates.
    Map must be on [3x3] grid.
    '''
    map = [
        [None, None, None],
        [None, None, None],
        [None, None, None]
    ]
    for i in range(len(locations)):
        x, y = locations[i]['coords']
        map[x][y] = i + 1

    '''
    Rendering is quite difficult, requires iterating and filling with spaces.
    '''
    previous_last = ' ' * 9
    for i in range(len(map)):
        render = [
            '',
            '',
            '',
            ''
        ]
        for j in range(len(map)):
            '''
            Creating tiles. You have to fill with whitespace too.
            '''
            if map[i][j]:
                test = list(render[1])
                if render[0] == '' or test[-1] == ' ':
                    if render != '' or test[-1] == ' ':
                        render[0] = render[0][:-1]
                        render[1] = render[1][:-1]
                        render[2] = render[2][:-1]
                        render[3] = render[3][:-1]
                    render[0] += f'  '
                    render[1] += f' |'
                    render[2] += f' |'
                    render[3] += f' |'

                render[0] += f'___ '
                render[1] += f'   |'
                render[2] += f' {map[i][j]} |'
                render[3] += f'___|'
            else:
                render[0] += f'    '
                render[1] += f'    '
                render[2] += f'    '
                render[3] += f'    '
        '''
        Rendering.
        '''
        for index, line in enumerate(render):
            if i == 0 and index == 0:
                print(line)
            elif index == 0:
                print(merge_renders(previous_last, line))
            elif i == len(map) - 1:
                print(line)
            elif index == len(render) - 1:
                previous_last = line
            else:
                print(line)
    '''
    Description.
    '''
    print()
    for index, location in enumerate(locations):
        location_name = location['name']
        print_string = f'{index + 1}. {location_name}'
        if index == current_location:
            print_string += ' (tutaj jesteś)'
        print(print_string)
    print()
