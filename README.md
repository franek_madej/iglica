# Iglica

# Requirements

* menu,
* pomoc,
* co najmniej 3 poziomy,
* na każdym z poziomów ma być co najmniej 5 lokacji
    * po co najmniej 2-3 zadania (przykładowe lokacje: rynek, las itp., przykładowe zadania w lokacji: pójść do lasu pokonać potwora i uwolnić porwaną osobę, wygrać partię w kółko i krzyżyk lub inną grę z przeciwnikiem, który będzie komputerem itp.)
* gra ma umożliwiać stworzenie postaci
* wylosowanie wartości cech
* zapis stanu gry
* zaimplementowanie mapy przejść np. w formie zagnieżdżonych struktur

Strona techniczna. W grze należy wykorzystać następujące elementy:

* struktury:
    * listy,
    * krotki,
    * słowniki,
* zapis i odczyt z/do pliku,
* funkcje,
* pętle,
* obiekty,
* instrukcje warunkowe.

# Development enviroment setup

* install `pip` / `pipenv`
* install `inquirer` and `tqdm`

alternative way:

go to https://ide.c9.io/kosciak9/iglica and open editor
